package com.lvmoney.frame.ai.seetaface.common.constant;/**
 * 描述:
 * 包名:com.lvmoney.frame.ai.seetaface.common.constant
 * 版本信息: 版本1.0
 * 日期:2022/2/10
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2022/2/10 16:03
 */
public class SeetafaceCommonConstant {
    /**
     * 人脸识别默认桶名称
     */
    public final static String DEFAULT_BUCKET_NAME = "seetaface";

    /**
     * 未删除
     */
    public static final String VALID_NOT_DELETE = "1";

    /**
     * age_predictor.csta
     */
    public static final String CSTA_AGE_PREDICTOR = "age_predictor.csta";
    /**
     * eye_state.csta
     */
    public static final String CSTA_EYE_STATE = "eye_state.csta";
    /**
     * face_detector.csta
     */
    public static final String CSTA_FACE_DETECTOR = "face_detector.csta";

    /**
     * face_landmarker_mask_pts5.csta
     */
    public static final String CSTA_FACE_LANDMARKER_MASK_PTS5 = "face_landmarker_mask_pts5.csta";

    /**
     * face_landmarker_pts5.csta
     */
    public static final String CSTA_LANDMARKER_PTS5 = "face_landmarker_pts5.csta";

    /**
     * face_landmarker_pts68.csta
     */
    public static final String CSTA_LANDMARKER_PTS68 = "face_landmarker_pts68.csta";

    /**
     * face_recognizer.csta
     */
    public static final String CSTA_FACE_RECOGNIZER = "face_recognizer.csta";
    /**
     * face_recognizer_mask.csta
     */
    public static final String CSTA_FACE_RECOGNIZER_MASK = "face_recognizer_mask.csta";
    /**
     * fas_first.csta
     */
    public static final String CSTA_FAS_FIRST = "fas_first.csta";
    /**
     * fas_second.csta
     */
    public static final String CSTA_FAS_SECOND = "fas_second.csta";
    /**
     * gender_predictor.csta
     */
    public static final String CSTA_GENDER_PREDICTOR = "gender_predictor.csta";
    /**
     * mask_detector.csta
     */
    public static final String CSTA_MASK_DETECTOR = "mask_detector.csta";
    /**
     * pose_estimation.csta
     */
    public static final String CSTA_POSE_ESTIMATION = "pose_estimation.csta";
    /**
     * quality_lbn.csta
     */
    public static final String CSTA_QUALITY_LBN = "quality_lbn.csta";


}
